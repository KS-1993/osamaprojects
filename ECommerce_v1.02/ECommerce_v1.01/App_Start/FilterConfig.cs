﻿using System.Web;
using System.Web.Mvc;

namespace ECommerce_v1._01
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
