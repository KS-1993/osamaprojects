﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECommerce_v1._01.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Catagory { get; set; }
        public string Type { get; set; }
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
        public double Price { get; set; }
        public string Summery { get; set; }
    }
}