﻿using ECommerce_v1._01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EcommerceLibrary;

namespace ECommerce_v1._01.Controllers
{
    public class ProductController : Controller
    {
        product Prd = new product();
        List<Product> ProductList = new List<Product>();
        // GET: Product
        public ActionResult Index()
        {
            var TenProducts = Prd.RetreiveHome();
            foreach (var item in TenProducts)
            {
                ProductList.Add(new Product { Id = item.Id, Catagory = item.Catagory, Type = item.Type, ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery });
            }
            return View(ProductList);
        }
        public ActionResult SearchProduct()
        {
            return View();
        }
        public ActionResult SearchResults(string Search)
        {
            var SearchResult = Prd.RetreiveSearch(Search);
            foreach (var item in SearchResult)
            {
                ProductList.Add(new Product { Id = item.Id, Catagory = item.Catagory, Type = item.Type, ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery });
            }
            return View(ProductList.ToList())   ;
        }
        public ActionResult Details(int ID)
        {
            var ProductDetail = Prd.RetreiveDetail(ID);
            foreach (var item in ProductDetail)
            {
                ProductList.Add(new Product { Id = item.Id, Catagory = item.Catagory, Type = item.Type, ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery });
            }
            return View(ProductList[0]);
        }
        public ActionResult MenPage()
        {
            var MenList = Prd.RetreiveMen();
            foreach (var item in MenList)
            {
                    ProductList.Add(new Product { Id = item.Id , Catagory = item.Catagory , Type = item.Type , ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery});
            }
            return View(ProductList.ToList());
        }
        public ActionResult WomenPage()
        {
            var WomensList = Prd.RetreiveWomen();
            foreach (var item in WomensList)
            {
                ProductList.Add(new Product { Id = item.Id, Catagory = item.Catagory, Type = item.Type, ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery });
            }
            return View(ProductList.ToList());
        }
        public ActionResult KidsPage()
        {
            var KidsList = Prd.ReteiveKids();
            foreach (var item in KidsList)
            {
                ProductList.Add(new Product { Id = item.Id, Catagory = item.Catagory, Type = item.Type, ProductCode = item.ProductCode, Price = item.Price, Summery = item.Summery });
            }
            return View(ProductList);
        }
    }
}